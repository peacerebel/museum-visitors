## Museum Visitors Data Service

Simple node.js service fetches some analytical data on museum visitors from [LA Museum Open Data API](https://data.lacity.org/Arts-Culture/Museum-Visitors/trxm-jn3c)

#### Installation

* Node version: `16.13.1`

* Typescript

```sh
npm install -g typescript # npm i -g typescript
```

* Install dependencies

```sh
npm install # or npm i
```

* Start server

Default port: 3000
```sh
npm start
```

#### Example

* Request:

```sh
curl "http://localhost:3000/api/visitors?date=1633844913345&ignore=avila_adobe"
```


* Response:

```json
{
  "year": 2021,
  "month": 10,
  "highest": {
    "museum": "iamla",
    "visitors": "515"
  },
  "lowest": {
    "museum": "gateway_to_nature_center",
    "visitors": "0"
  },
  "ignored": {
    "museum": "avila_adobe",
    "visitors": "7201"
  },
  "total": 6933
}
```
