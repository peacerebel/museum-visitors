export interface GetVisitorsDataInput {
  date: Date
  ignore?: string
}
