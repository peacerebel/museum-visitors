import { Request, Response, NextFunction } from "express"
import * as moment from "moment"
import { GetVisitorsDataInput } from "../interface/visitors.interface";
import { VisitorsService } from "../services/visitors.service";

export class VisitorsController {
  visitorsService = new VisitorsService()

  async getVisitorsData(request: Request, response: Response, next: NextFunction) {
    const { query: params } = request
    let parsedDate

    try {
      parsedDate = new Date(parseInt(params.date))
      parsedDate = moment(parsedDate).startOf('month').format('YYYY-MM-DDTHH:mm:ss.SSS')
    }
    catch (err) {
      throw new Error(`Invalid Date ${params.date}`)
    }

    const input: GetVisitorsDataInput = {
      date: parsedDate,
      ignore: params.ignore
    }

    return this.visitorsService.getVisitorsForMonth(input)
  }
}
