import axios from "axios"
import { stringify } from "querystring"
import { head, omit } from "lodash"

export class LAMuseumDataService {

  private async getDataFromService(url: string) {
    const instance = axios.create({
      method: "get",
      baseURL: "https://data.lacity.org/resource"
    });

    return instance(url);
  }

  async getVisitorsDataForMonth(input) {
    const url = `trxm-jn3c.json?${stringify(input)}`

    try {
      const response = await this.getDataFromService(url)

      return this.parseReponseData(head(response.data))
    }
    catch (err) {
      return Promise.reject(this.handleError(err))
    }
  }

  private handleError(err) {
    return err.response?.data || err.request?.data || err;
  }

  private parseReponseData(data) {
    const OMITTED_FIELDS = ["month"]
    const museums = Object.keys(data).filter(m => !OMITTED_FIELDS.includes(m))

    return {
      date: data.month,
      museums: museums.map(m => ({
        museum: m,
        visitors: data[m]
      }))
    }
  }
}
