import { GetVisitorsDataInput } from "../interface/visitors.interface";
import { LAMuseumDataService } from "../services/LA-museum-data-api.service";
import { head, sortBy, reverse } from "lodash"
import moment = require("moment");

export class VisitorsService {
  laMuseumDataService = new LAMuseumDataService()

  sortMuseumsByVisitorsCount(museums) {
    return sortBy(museums, ['visitors'])
  }

  getMostVisitedMuseum(museums) {
    return head(reverse(this.sortMuseumsByVisitorsCount(museums)))
  }

  getLeastVisitedMuseum(museums) {
    return head(this.sortMuseumsByVisitorsCount(museums))
  }

  getTotalVisitors(museums) {
    return museums.reduce((sum, museum) => {
      return sum + parseInt(museum.visitors)
    }, 0)
  }

  getYearFromDate(date) {
    return moment(date).get('year');
  }

  getMonthFromDate(date) {
    return moment(date).get('month') + 1;
  }

  async getVisitorsForMonth(input: GetVisitorsDataInput) {
    const dataForRequestedMonth = await this.laMuseumDataService.getVisitorsDataForMonth({ month: input.date })

    const museumIgnored = dataForRequestedMonth.museums.filter(m => m.museum === input.ignore)
    const museumsAfterIgnore = dataForRequestedMonth.museums.filter(m => m.museum !== input.ignore)


    return {
      year: this.getYearFromDate(dataForRequestedMonth.date),
      month: this.getMonthFromDate(dataForRequestedMonth.date),
      highest: this.getMostVisitedMuseum(museumsAfterIgnore),
      lowest: this.getLeastVisitedMuseum(museumsAfterIgnore),
      ignored: head(museumIgnored),
      total: this.getTotalVisitors(museumsAfterIgnore)
    }
  }
}
