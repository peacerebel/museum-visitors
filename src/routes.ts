import { VisitorsController } from './controller/visitors-controller'

export const Routes = [
  {
    method: "get",
    route: "/api/visitors",
    controller: VisitorsController,
    action: "getVisitorsData"
  }
]
