import * as express from "express"
import * as cors from "cors"
import { Request, Response } from "express"
import { Routes } from './routes'

const PORT = 3000

const app = express()

app.use(cors())

//Register routes
Routes.forEach((route) => {
  (app)[route.method](route.route, (req: Request, res: Response, next: Function) => {
    const result = new (route.controller)()[route.action](req, res, next)

    if (result instanceof Promise) {
      result
        .then((result) => (result !== null && result !== undefined ? res.send(result) : res.end()))
        .catch(error => res.status(error.status || 500).send(error))
    }
    else if (result !== null && result !== undefined) {
      res.json(result)
    }
    else {
      res.end()
    }
  })
})

// Start server
app.listen(PORT)
console.log(`Server started on ${PORT}`)
